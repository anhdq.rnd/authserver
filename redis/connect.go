package redis

import "github.com/go-redis/redis"

func NewClient(local bool) *redis.Client {
	var redisClient *redis.Client
	if !local {
		redisClient = redis.NewClient(&redis.Options{
			//Addr:     "localhost:6379",
			Addr:     "redis:6379",
			Password: "bbf5099c19728139a0c0bc77de98e241ddbe13a6f923b3557a6a035e45f05d1c27ec728af982b629b682d47fc043db5b48e2573880f3da3be527782ab7246181", // no password
			DB:       0,                                                                                                                                  // default DB
		})
	} else {
		redisClient = redis.NewClient(&redis.Options{
			Addr: "localhost:6379",
			//Addr:     "redis:6379",
			//Password: "bbf5099c19728139a0c0bc77de98e241ddbe13a6f923b3557a6a035e45f05d1c27ec728af982b629b682d47fc043db5b48e2573880f3da3be527782ab7246181", // no password
			DB: 0, // default DB
		})
	}

	return redisClient
}
