package models

type UserDeviceToken struct {
	Id          int    `json:"id"`
	UserId      string `json:"user_id"`
	DeviceToken string `json:"device_token"`
}
