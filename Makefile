
GOPATH:=$(shell go env GOPATH)


.PHONY: proto
proto:
.PHONY: build
build:
	GOARCH=amd64 CGO_ENABLED=0 GOOS=linux go build -o authserver main.go
.PHONY: test
test:
	go test -v ./... -cover

.PHONY: docker
docker:
	docker build . -t authserver:latest
run:
	docker run --net=host -p 50052:50050  -e MICRO_SERVER_ADDRESS=:50050 -e MICRO_REGISTRY=mdns authserver
